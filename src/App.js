import React, { Component } from 'react';
import Firebase from 'firebase';

const FirebaseConfig = {
  apiKey: "AIzaSyCponvoTwgCrND6sSEsKPmg6OWJ6pD6Prw",
  authDomain: "dashboard-app-fdbd5.firebaseapp.com",
  databaseURL: "https://dashboard-app-fdbd5.firebaseio.com",
  storageBucket: "dashboard-app-fdbd5.appspot.com",
  messagingSenderId: "923812646132"
};

Firebase.initializeApp(FirebaseConfig);

const Title = ({ totalCount }) => {
  return (
    <div>
      <h1>Todos: { totalCount }</h1>
    </div>
  );
}

const TodoForm = ({ addTodo }) => {
  // input tracker
  let input;

  return (
    <div>
      <input ref={node => {
        input = node
      }} />
      <button onClick={() => {
        addTodo(input.value);
        input.value = '';
      }}> + </button>
    </div>
  );
}

const Todo = ({ todo, remove }) => {
  // each todo
  return (
    <li onClick={() => { remove(todo.id)} }>{todo.id} : {todo.item}</li>
  );
}

const TodoList = ({ todos, remove }) => {
  let todosList = Firebase.database().ref('todos').orderByKey();

  todosList.once("value")
    .then(snapshot => {
      
      snapshot.forEach(childSnapshot => {
        // childData will be the actual contents of the child
        todos.push(childSnapshot.val());
    });
  });

  // map through each item
  const todoNode = todos.map((todo) => {
    return (
      <Todo todo={todo} key={todo.id} remove={remove} />
    );
  });

  return (
    <ul>{todoNode}</ul>
  );
}

window.id = 0;
class App extends Component {

  constructor(props) {
    // pass props to parent class
    super(props);

    // set initial state
    this.state = {
      todos: []
    }

    // get firebase root
    this.firebaseRefRoot = Firebase.database();
  }

  // addTodo handler
  addTodo(val) {
    // assemble data
    // define new todo with the value you entered
    const todo = { id: Date.now(), text: val }

    // update data
    // push the newly created todo into the data array created in the constructor
    //this.state.data.push(todo);
    this.firebaseRefRoot.ref('todos').push({ id: todo.id, item: todo.text });

    // update state
    // update the react state with the newly added data so it can be used by children?
    this.setState({ todos: this.state.todos });
  }

  // handle remove
  handleRemove(id) {
    // filter all todos except for the one with a matching id
    const remainder = this.state.data.filter((todo) => {
      if (todo.id !== id) return todo;
    });

    // update state with filter
    // update data array with all todos that didn't match the id
    this.setState({ data: remainder });
  }

  // fires before anything renders
  componentWillMount() {
    this.firebaseRef = Firebase.database().ref('todos');

    let todos = [];

    // load all current content on page load
    this.firebaseRef.once("value")
      .then(snapshot => {
        snapshot.forEach((data) => {
          var todo = {
            id: data.val().id,
            item: data.val().item
          }

          todos.push(todo);
          this.setState({
            todos: todos,
            totalCount: todos.length
          });
        });
    });

    console.log(todos);
  }

  render() {
    return (
      <div>
        <Title totalCount={ this.state.totalCount }/>
        <TodoForm addTodo={ this.addTodo.bind(this) } />
        <TodoList
          todos={ this.state.todos }
          remove={ this.handleRemove.bind(this) }/>
      </div>
    );
  }
}

export default App;
