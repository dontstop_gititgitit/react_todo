var path = require('path');
var webpack = require('webpack');

var buildDir = path.resolve(__dirname, '/public');
var appDir = path.resolve(__dirname, '/app');

var config = {
  entry: APP_DIR + '/index.jsx',
  output: {
    path: buildDir,
    filename: 'bundle.js'
  },
  module : {
    loaders : [
      {
        test : /\.jsx?/,
        include : appDir,
        loader : 'babel'
      }
    ]
  }
};